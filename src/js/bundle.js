"use strict";

var _imask = _interopRequireDefault(require("imask"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var validate = require('validate.js'); // FEATURES


var featuresDl = document.querySelector('.features__dl'),
    featuresDd = featuresDl.querySelectorAll('.features__dd'),
    featuresItems = document.querySelectorAll('.features__item'),
    featuresLabels = document.querySelectorAll('.features__label');
var featuresDlPresence = !(getComputedStyle(featuresDl).display === 'none');
var featuresDdHeights = [];

function clearHeights(el) {
  el.forEach(function (el) {
    el.style.height = 0;
  });
}

if (!featuresDlPresence) {
  (function () {
    for (var i = 0; i < featuresDd.length; i++) {
      featuresDl.style.display = 'block';
      featuresDdHeights.push(featuresDd[i].clientHeight);
      var newDd = featuresDd[i].cloneNode(true);
      featuresItems[i].appendChild(newDd);
    }

    featuresDl.style.display = 'none';
    var innerDd = document.querySelectorAll('.features__dd');
    clearHeights(innerDd);

    var _loop = function _loop(_i) {
      var inner = featuresLabels.item(_i).parentNode.querySelector('.features__dd');
      featuresLabels.item(_i).addEventListener('click', function () {
        clearHeights(innerDd);
        inner.style.height = featuresDdHeights[_i] + 'px';
      });
    };

    for (var _i = 0; _i < featuresLabels.length; _i++) {
      _loop(_i);
    }
  })();
} // FORM
// masked input


var phoneMask = new _imask.default(document.querySelector('input[type="tel"]'), {
  mask: '+{7} (000) 000-00-00',
  // mask: '+ 7 (000) 000-00-00',
  // mask: '+ 7 ',
  // lazy: false,  // make placeholder always visible
  placeholderChar: ' ' // defaults to '_'

});
var mainForm = document.querySelector('.form'),
    submitBtn = mainForm.querySelector('button[type="submit"]'),
    formInputs = mainForm.querySelectorAll('input'),
    formRadio = mainForm.querySelectorAll('input[type="radio"]');
submitBtn.classList.add('disabled'); // правила проверки для validate.js

var constraints = {
  name: {
    presence: true,
    length: {
      minimum: 1
    }
  },
  tel: {
    presence: true,
    length: {
      minimum: 18
    }
  },
  email: {
    presence: true,
    email: true
  },
  features: {
    presence: true
  } // добавить убрать классы .error и .success

};

function showErrors(input, errors) {
  if (errors) {
    input.classList.add('error');
    input.classList.remove('success');
  } else {
    input.classList.add('success');
    input.classList.remove('error');
  }
} // проверка формы


function checkForm() {
  var errors = validate(mainForm, constraints);

  if (!errors) {
    submitBtn.classList.remove('disabled');
    return true;
  } else {
    submitBtn.classList.add('disabled');
  }
} // подготовка формы перед отправкой


function submitForm() {
  if (checkForm()) {
    // id радио кнопки
    var radioId;

    for (var i = 0; i < formRadio.length; i++) {
      if (formRadio.item(i).checked) {
        radioId = formRadio.item(i).id;
      }
    }

    var formValues = validate.collectFormValues(mainForm); // поменять значение features на id радио кнопки

    formValues.features = radioId; // вывести данные в json

    var output = JSON.stringify(formValues);
    console.log("form values: ".concat(output));
  }
} // слушать кнопку формы


mainForm.addEventListener('submit', function (e) {
  e.preventDefault();
  submitForm();
}); // слушать инпуты формы

var _loop2 = function _loop2(i) {
  ['change', 'blur'].forEach(function (evt) {
    formInputs.item(i).addEventListener(evt, function () {
      var errors = validate(mainForm, constraints) || {};
      checkForm();
      showErrors(this, errors[this.name]);
    });
  });
};

for (var i = 0; i < formInputs.length; i++) {
  _loop2(i);
}
//# sourceMappingURL=bundle.js.map
