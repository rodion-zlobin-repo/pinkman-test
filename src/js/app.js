
import IMask from 'imask';
const validate = require('validate.js');


// FEATURES

const featuresDl = document.querySelector('.features__dl'), 
			featuresDd = featuresDl.querySelectorAll('.features__dd'),
			featuresItems = document.querySelectorAll('.features__item'),
			featuresLabels = document.querySelectorAll('.features__label');

const featuresDlPresence = !(getComputedStyle(featuresDl).display === 'none');
let featuresDdHeights = [];

function clearHeights(el) {
	el.forEach(function(el){
		el.style.height = 0;
	});
}


if (!featuresDlPresence) {
	
	for (let i = 0; i < featuresDd.length; i++) {
		featuresDl.style.display = 'block';
		featuresDdHeights.push(featuresDd[i].clientHeight);
		const newDd = featuresDd[i].cloneNode(true);
		featuresItems[i].appendChild(newDd);
	}
	featuresDl.style.display = 'none';
	
	const innerDd = document.querySelectorAll('.features__dd');
	clearHeights(innerDd);

	for (let i = 0; i < featuresLabels.length; i++) {
		const inner = featuresLabels.item(i).parentNode.querySelector('.features__dd');
		featuresLabels.item(i).addEventListener('click', function(){
			clearHeights(innerDd);
			inner.style.height = featuresDdHeights[i] + 'px';
		});
	}
	
}





// FORM


// masked input

const phoneMask = new IMask(document.querySelector('input[type="tel"]'), {
  mask: '+{7} (000) 000-00-00',
  // mask: '+ 7 (000) 000-00-00',
  // mask: '+ 7 ',
  // lazy: false,  // make placeholder always visible
  placeholderChar: ' '     // defaults to '_'
});


const mainForm = document.querySelector('.form'),
			submitBtn = mainForm.querySelector('button[type="submit"]'),
			formInputs = mainForm.querySelectorAll('input'),
			formRadio = mainForm.querySelectorAll('input[type="radio"]');

submitBtn.classList.add('disabled');



// правила проверки для validate.js

var constraints = {
	name: {
		presence: true,
		length: {
			minimum: 1
		}
	},
	tel: {
		presence: true,
		length: {
			minimum: 18
		}
	},
	email: {
		presence: true,
		email: true
	},
	features: {
		presence: true,
	}
}


// добавить убрать классы .error и .success

function showErrors(input, errors) {
	if (errors) {
		input.classList.add('error');
		input.classList.remove('success');
	} else {
		input.classList.add('success');
		input.classList.remove('error');
	}
}


// проверка формы

function checkForm() {
	var errors = validate(mainForm, constraints);
	if (!errors) {
		submitBtn.classList.remove('disabled');
		return true;
	} else {
		submitBtn.classList.add('disabled');
	}
}


// подготовка формы перед отправкой

function submitForm() {
	if (checkForm()) {

		// id радио кнопки
		let radioId;
		for (let i = 0; i < formRadio.length; i++) {
			if (formRadio.item(i).checked) {
				radioId = formRadio.item(i).id;
			}
		}

		const formValues = validate.collectFormValues(mainForm);

		// поменять значение features на id радио кнопки
		formValues.features = radioId;

		// вывести данные в json
		const output = JSON.stringify(formValues);
		console.log(`form values: ${output}`);
	}
}


// слушать кнопку формы

mainForm.addEventListener('submit', function(e){
	e.preventDefault();
	submitForm();
});


// слушать инпуты формы

for (let i = 0; i < formInputs.length; i++) {
	['change', 'blur'].forEach(function(evt){
		formInputs.item(i).addEventListener(evt, function() {
			const errors = validate(mainForm, constraints) || {};
			checkForm();
			showErrors(this, errors[this.name]);
		});
	});
}
