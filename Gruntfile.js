
module.exports = function (grunt) {

  const sass = require('node-sass');
   
  require('load-grunt-tasks')(grunt);

  grunt.initConfig({


    // PUG

    pug: {

      // pug dev
      dev: {
        files: {
          './src/index.html': './src/views/index.pug'
        },
        options: {
          pretty: true
        }
      },

      // pug dist
      dist: {
        files: {
          './dist/index.html': './src/views/index.pug'
        },
        options: {
          pretty: true
        }
      }
    }, // pug


    // SASS

    sass: {

      // dev sass
      dev: {
        options: {
          implementation: sass,
          sourceMap: true,
          style: 'expanded',
          update: true
        },
        files: {
          './src/css/style.css': './src/scss/style.scss'
        }
      },

      // dist sass
      dist: {
        options: {
          implementation: sass,
          sourceMap: false,
          style: 'expanded',
          update: true
        },
        files: {
          './dist/css/style.css': './src/scss/style.scss'
        }
      }
    }, // lib sass



    // POSTCSS

    postcss: {
      
      // dev postcss
      dev: {
        options: {
          processors: [
            require('autoprefixer')({browsers: ['last 2 versions']} )
          ]

        },
        src: './src/css/*.css',
      }, // dev

      // dist postcss
      dist: {
        options: {
          processors: [
            require('autoprefixer')({browsers: ['last 2 versions']} ),
            require('cssnano')()
          ]

        },
        src: './dist/css/*.css',
      } // dist
    }, // postcss


    // BABEL

    babel: {
      dev: {
        options: {
          sourceMap: true,
          presets: ['@babel/preset-env']
        },
        files: {
          "src/js/bundle.js": "src/js/app.js"
        }
      }
    }, // babel


    // BROWSERIFY

    browserify: {
      dev: {
        options: {
          debug: true
        },
        files: {
          './src/js/bundle.js': './src/js/bundle.js'
        }
      },
      dist: {
        files: {
          './dist/js/bundle.js': './src/js/bundle.js'
        }
      }
    }, // browserify


    // UGLIFY

    uglify: {
      dist: {
        files: {
          './dist/js/bundle.js': './dist/js/bundle.js'
        }
      }
    }, // uglify


    // WATCH

    watch: {
      css: {
        files: './src/scss/**/*.scss',
        tasks: ['sass:dev']  
      },
      pug: {
        files: './src/views/*.pug',
        tasks: ['pug:dev']
      },
      scripts: {
        files: './src/js/*.js',
        tasks: ['babel:dev', 'browserify:dev']
      }
    }, // watch


    // BROWSER SYNC

    browserSync: {
      dev: {
        bsFiles: {
          src: [
            './src/css/*.css',
            './src/*.html',
            './src/js/*.js'
          ]
        },
        options: {
          watchTask: true,
          server: './src',
          browser: 'chrome'
        }
      }
    } // br sync


  });


  grunt.loadNpmTasks('grunt-contrib-pug');
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-postcss');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-browser-sync');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  // grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-babel');
  grunt.loadNpmTasks('grunt-browserify');


  grunt.registerTask('scripts', ['babel:dev', 'browserify:dev']);
  
  grunt.registerTask('start', ['browserSync', 'watch']);
  
  grunt.registerTask('default', ['pug:dev', 'sass:dev', 'postcss:dev', 'babel:dev', 'browserify:dev']);
  
  grunt.registerTask('build', ['pug:dist', 'sass:dist', 'postcss:dist', 'babel:dev', 'browserify:dist', 'uglify:dist']);

};
